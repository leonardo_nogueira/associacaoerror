<%-- 
    Document   : layout
    Created on : 29 de abr. de 2021, 21:31:11
    Author     : leola
--%>

<%@page import="com.mycompany.associacaoweb.model.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="icon" type="image/png" href="images/favicon.ico"  />
        <!--===============================================================================================-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous" />
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css" />
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/animsition@4.0.2/dist/css/animsition.min.css" />
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/layout.css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Associação Cultural</title>
    </head>
    <body>
        <header>
            <nav class="navbar navbar-inverse navbar navbar-dark bg-dark fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="btn btn-dark navbar-toggle pull-left menu-toggle"  data-target="#side-bar">
                            <i class="oi oi-menu"></i>
                        </button>
                        <a class="navbar-brand " href="#">Associação Cultural</a>
                    </div>
                </div>
            </nav>
        </header>

        <div class="layout-main"> 

            <aside class="wrapper" id="side-bar">
                <nav id="menu" class="sidebar sidebar-open">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link " href="control?ac=home"> <i
                                    class="oi oi-home"></i> <span>Home</span>
                            </a></li>
                    </ul>
                    
                        <ul class="nav nav-pills" sec:authorize="hasRole('ADMIN')" >
                            <li class="nav-item"><span class="nav-link active" >Administração</span></li>
                         
                            <li class="nav-item">
                                <a class="nav-link"
                                   href="#"> <i class="oi oi-cart"></i> <span>Eventos</span>
                                </a>
                                <li class="nav-item"><span class="nav-link active" >Cadastrar Evento</span></li>
                            </li>                           
                        </ul>
                    <ul class="nav nav-pills">
                        <li class="nav-item"><span class="nav-link active" >Geral</span></li>
                        <li class="nav-item">
                            <a class="nav-link " href="#"> <i class="oi oi-spreadsheet"></i>
                                    <span>Meus Dados</span>
                            </a>
                        </li>
                        

                        <li class="nav-item" >
                            <a class="nav-link " href="control?ac=logout"> 
                                <i class="oi oi-account-logout"></i> <span>Sair</span>
                            </a>
                        </li>

                    </ul>

                </nav>
            </aside>
            
            <% 
                Usuario u = (Usuario)session.getAttribute("user");
            %>

            
            
           

        </div>
        <footer class="layout-footer">
            <div class="container">
                <span class="footer-copy">&copy; 2021 Associação Cultural. Todos os direitos reservados.</span>
            </div>
        </footer> 
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" ></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
        <!--===============================================================================================-->
        <script src="https://cdn.jsdelivr.net/npm/animsition@4.0.2/dist/js/animsition.min.js"></script>
        <!--===============================================================================================-->
    </body>
</html>
