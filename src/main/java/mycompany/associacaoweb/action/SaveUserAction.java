/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycompany.associacaoweb.action;

import com.mycompany.associacaoweb.controllergenerics.GenericCommander;
import com.mycompany.associacaoweb.model.Usuario;
import com.mycompany.associacaoweb.model.dao.UsuarioDao;
import java.io.IOException;
import java.util.HashSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author leola
 */
public class SaveUserAction extends GenericCommander {

    public SaveUserAction(boolean isLogado) {
        super(isLogado);
    }

    @Override
    public void executa(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
 


 Usuario user= new Usuario(0, 
         request.getParameter("cpNome"), 
         request.getParameter("cpLogin"), 
         request.getParameter("cpPassword"), 
         false );
 
       
        UsuarioDao.getCon().getTransaction().begin();
        UsuarioDao.getCon().persist(user);
        UsuarioDao.getCon().getTransaction().commit();
        
        request.setAttribute("msg", "Usuario criado com sucesso!");
        
        
        new ViewLoginAction(false).executa(request, response);
    }
    
}
